﻿using System;

namespace TestLib
{
    public static class Math
    {
        public static Int32 Sum(Int32 _a, Int32 _b)
        {
            return _a + _b;
        }
    }
}